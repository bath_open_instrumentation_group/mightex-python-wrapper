from __future__ import annotations
import logging
import re
import threading
from typing import Optional, Union, List

import serial

class MightexController:
    """A class to control a Mightex 12 or 16 channel LED driver
    
    This uses `pyserial` internally, so is suitable for RS232 controlled
    units.

    Pretty much all the functionality is on a per-channel basis. This is
    accessed via `MightexChannel` objects, which can be obtained from the 
    controller either using `controller.channel(i)` for a single channel,
    or `controller.channels` to get a list of all of them.
    """
    _ser = None
    _ser_lock = None
    termination = "\r\n"
    _n_channels = None

    def __init__(self, port: str = "/dev/ttyUSB0", n_channels: int = 16):
        self._ser = serial.Serial(port, 9600, timeout=1)
        self._ser_lock = threading.RLock()
        self.query("ECHOOFF")
        # Query the device and match its model number
        m = self.query("DEVICEINFO", ".*SLC-([A-Z]{2})([\d]{2})-([A-Z])")
        self._n_channels = int(m.group(2))
        print(f"Connected to a {m.group(0)} with {self._n_channels} channels.")

    def ensure_port_is_open(self):
        if not self._ser:
            raise IOError("The serial port is missing!")
        if not self._ser.is_open:
            logging.warning("The serial port was opened implicitly - should do this explicitly")
            self._ser.open()

    def query(self, message: str, response_pattern: Optional[str]=None) -> Union[str, re.Match]:
        """Send a message to the box, and read the reply
        
        `message` is the string to send - it should not be terminated with
        CRLF, that's done by this function.
        
        `response_pattern` should be a regular expression. If provided, we
        will attempt to match the response with the pattern, and return a
        match object (from `re.match(response_pattern, response)`). If the
        match fails, we raise a hopefully-helpful IOError.
        """
        self.ensure_port_is_open()
        with self._ser_lock:
            self._ser.reset_input_buffer()
            self._ser.write((message + self.termination).encode())
            response = self._ser.readline().decode().rstrip(" \r\n")
        if response_pattern is None:
            return response
        else:
            m = re.match(response_pattern, response)
            if m:
                return m
            else:
                raise IOError(
                    "The serial device responded unexpectedly.\n"
                    f"    Sent: '{message}'\n"
                    f"    Received: '{response}'\n"
                    f"    Pattern: '{response_pattern}"
                )

    @property
    def device_info(self) -> str:
        return self.query("DEVICEINFO")

    @property
    def n_channels(self) -> int:
        """The number of channels this device has"""
        return self._n_channels

    @property
    def channels(self) -> List[MightexChannel]:
        """A list of `MightexChannel` objects representing all the channels."""
        return [self.channel(i+1) for i in range(self.n_channels)]

    def channel(self, i: int) -> MightexChannel:
        """The channel with index i, as a `MightexChannel` object"""
        if i > self.n_channels or i < 1:
            raise KeyError(f"Channels are numbered from 1 to {self.n_channels}")
        return MightexChannel(self, int(i))

CHANNEL_MODES = ("disable", "normal", "strobe", "trigger")

class MightexChannel:
    """A class to control one channel of the Mightex driver box"""
    _controller = None
    channel_number = None

    def __init__(self, controller: MightexController, number: int):
        self._controller = controller
        self.channel_number = number

    def query(
        self, 
        message: str, 
        response_pattern: Optional[str] = None
    )  -> Union(str, re.Match):
        return self._controller.query(message, response_pattern=response_pattern)

    @property
    def mode(self) -> str:
        """The current working mode of the channel.  
        
        This sets how the channel will operate. Valid values are:
        "disable", "normal", "strobe", "trigger"
        """
        response = self.query(f"?MODE {self.channel_number}", "#([\d])")
        index = int(response.group(1))
        return CHANNEL_MODES[index]

    @mode.setter
    def mode(self, mode: str):
        if mode not in CHANNEL_MODES:
            raise ValueError(f"Channel mode must be one of {CHANNEL_MODES}")
        index = CHANNEL_MODES.index(mode)
        _response = self.query(f"MODE {self.channel_number} {index}")

    def current_query(self) -> re.Match:
        """Ask for, and match, working and maximum current when in normal mode"""
        return self.query(
            f"?CURRENT {self.channel_number}", 
            "^#([\d]+ )+([\d]+) ([\d]+)$"
            # The documentation says the return value should be # followed by four numbers.
            # Our unit seems to return 8 numbers, but (as per the docs) the last two seem
            # to be the maximum and working currents, in units of the current resolution.
            # These are accessible as groups 2 and 3 respectively.
        )

    @property
    def current(self) -> int:
        """The working current for normal mode, in mA (or 0.1mA)"""
        response = self.current_query()
        return int(response.group(3))

    @current.setter
    def current(self, value: int):
        _response = self.query(f"CURRENT {self.channel_number} {int(value)}")

    @property
    def max_current(self) -> int:
        """The maximum current for normal mode, in mA (or 0.1mA)"""
        response = self.current_query()
        return int(response.group(2))

    @max_current.setter
    def max_current(self, value: int):
        working_current = self.current
        _response = self.query(f"NORMAL {self.channel_number} {int(value)} {working_current}")

