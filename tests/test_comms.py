import mightex_serial
import time

if __name__ == "__main__":
    m = mightex_serial.MightexController()
    print()
    print("Device info string:")
    print(m.device_info)
    print()
    for c in m.channels:
        print(f"Channel {c.channel_number}'s mode is '{c.mode}'")
    print()
    for c in m.channels[0:1]:
        print(f"Testing channel {c.channel_number}")
        print("Setting max current to 440mA, current to 35mA")
        c.max_current = 440
        c.current = 35
        time.sleep(0.5)
        print(f"Working current is {c.current}")
        print(f"Max current is {c.max_current}")
        print("Enabling normal mode")
        c.mode = "normal"
        time.sleep(0.5)
        print("Disabling")
        c.mode = "disable"
    print()
    
