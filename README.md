# Mightex Python Wrapper

This is intended to be a minimal but functional Python module that controls a Mightex 16-channel LED controller.  We have the RS232 version rather than the USB version, and so we will use `pyserial` to communicate with it.  The same command set also works for the USB version, but the SDK documentation suggests it should go via their library, which I have not attempted to set up.

## Installing

The easiest way to get it running is to install directly from gitlab:

```
pip install git+https://gitlab.com/bath_open_instrumentation_group/mightex-python-wrapper.git
```

You can install it in editable mode by cloning the repository, then running `pip install -e .` though you will need to be running a recent version of `pip`, as there's no `setup.py`.

## Using

Once installed, you can import and use the class.  Some example code is below:

```
import mightex_serial

controller = mightex_serial.MightexController()  # default port is "/dev/ttyUSB0"

for c in controller.channels:
    print(f"Channel {c.channel_number} is currently {c.mode}")

# Enable the first channel (NB channel_number will be 1, as it's 1-indexed)
controller.channels[0].max_current = 100
controller.channels[0].current = 25
controller.channels[0].mode = "normal"

# Disable it again
controller.channels[0].mode = "disabled"
```

I've not yet determined whether it's better to turn channels on and off using `mode` or `current`. My guess would be `current`.

## Limitations

I've only implemented `normal` mode so far: our controller doesn't support triggering and only allows two steps in the strobe profile, so we're unlikely to use either of these.  Merge requests are welcome if you have a different controller/application that makes this useful.

## Adding functionality

If there is functionality that's provided by your controoler and not covered by the methods I've implemented so far (e.g. `strobe` or `trigger` mode), you can always communicate with the controller using `controller.query()`.  With one argument, it will send the string to the device (adding a `CRLF` terminator), and return the response (with the CRLF removed).  This only works for single-line responses, more than that will leave subsequent lines sitting in the input buffer. I think that's only an issue if you read the strobe profile, which I've not tried. `query` can take a second argument, `response_pattern`, which is interpreted as a regular expression.  If it's present, we will try to match the response and return a `re.Match` object, i.e. the output of `re.match`. If the match fails, we raise an `IOError` with helpful details (i.e. the message, response, and pattern). This is used by most of the commands that read values, to make sure any errors that occur get picked up early.

## Serial protocol

From the SDK documentation, the port has:

* Baud rate: `9600`
* Data bits: `8`
* Parity: `None`
* Stop bits: `1`

These are the defaults for `pyserial` anyway, though it's wise to specify the baud rate explicitly.

There is a comment in the SDK about wiring, but whatever is required, it is compatible with our off-the-shelf USB-Serial cable. No null modem is needed.

Messages should always be terminated with CRLF, i.e. `b"\r\n". This means that `serial.Serial.readline()` works out of the box.
